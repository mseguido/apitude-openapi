openapi: 3.0.1
info:
  title: CRUISES B2B2C
  description: B2B2C Cruises api platform
  contact:
    email: B2B2C-Team@hotelbeds365.onmicrosoft.com
  license:
    name: B2B2C Creed
    url: http://www.hotelbeds.com
  version: "1.0.2"
servers:
- url: https://api.hotelbeds.com
  description: Generated server url
paths:
  /cruisesdistributionservice/1.0/availability/search:
    get:
      tags:
      - Cruises availability
      summary: Search for cruises availability
      description: Return availability result list
      operationId: searchAvailability
      parameters:
      - name: metadata
        in: query
        description: Get metadata flag
        required: false
        schema:
          type: boolean
      - name: searchDto
        in: query
        required: true
        schema:
          $ref: '#/components/schemas/SearchAvailabilityRequestDTO'
      - name: Api-key
        in: header
        description: Api-key
        required: true
        schema:
          type: string
      responses:
        "200":
          description: Success
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ResponseDTOSearchAvailabilityResponseDTO'
        "500":
          description: Failure
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ErrorDTO'
        "404":
          description: Not Found
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ErrorDTO'
        "403":
          description: Forbidden
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ErrorDTO'
        "204":
          description: No Content
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ErrorDTO'
        "401":
          description: Unauthorized
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ErrorDTO'
components:
  schemas:
    SearchAvailabilityRequestDTO:
      required:
        - month
        - year
      type: object
      properties:
        year:
          maximum: 3000
          minimum: 2020
          type: integer
          description: Year for travel
          format: int32
        month:
          maximum: 12
          minimum: 1
          type: integer
          description: Month for travel
          format: int32
        lineId:
          type: integer
          description: Cruise line identifier
          format: int32
        areaId:
          type: integer
          description: Area identifier
          format: int32
        nights:
          type: integer
          description: How many nights you want cruises to be for
          format: int32
        shipId:
          type: integer
          description: Ships
          format: int32
      description: GET .../availability/search request object.
    ErrorDTO:
      type: object
      properties:
        dateTime:
          type: string
          description: Time and date the error was thrown
          format: date-time
        code:
          type: string
          description: Error code
        message:
          type: string
          description: Error message
        description:
          type: string
          description: Error description
        serviceName:
          type: string
          description: The name of the service that threw this error
        isBadRequest:
          type: boolean
          description: The error is caused by a bad request from client, it's not
            due to an internal service
        traceId:
          type: string
          description: The trace ID
        fieldErrors:
          type: array
          description: List of errors caused by field validations
          items:
            $ref: '#/components/schemas/FieldErrorDTO'
        nestedError:
          $ref: '#/components/schemas/ErrorDTO'
    FieldErrorDTO:
      type: object
      properties:
        code:
          type: string
          description: The field error code
        field:
          type: string
          description: The name of the field with the error
        message:
          type: string
          description: The field error message
      description: List of errors caused by field validations
    CabinPriceDTO:
      type: object
      properties:
        cabinType:
          type: string
          description: The cabin type
        amount:
          type: number
          description: The price amount
      description: Cabin price info
    CruiseDTO:
      type: object
      properties:
        codeToCruiseId:
          type: integer
          format: int32
        cruiseId:
          type: integer
          format: int32
        name:
          type: string
        engine:
          type: string
        startDate:
          type: string
          format: date
        endDate:
          type: string
          format: date
        nights:
          type: integer
          format: int32
        sailDate:
          type: string
          format: date
        sailNights:
          type: integer
          format: int32
        returnDate:
          type: string
          format: date
        groupIds:
          type: string
        priority:
          type: integer
          format: int32
        privateNotes:
          type: string
        resultWeight:
          type: integer
          format: int32
        roundTrip:
          type: boolean
        seaDays:
          type: integer
          format: int32
        senior:
          type: boolean
        soldOut:
          type: integer
          format: int32
        special:
          type: boolean
        specialsGroup:
          type: string
        stopLive:
          type: boolean
        voyageCode:
          type: string
        lineId:
          type: integer
          description: Line identifier
          format: int32
        ports:
          type: array
          items:
            $ref: '#/components/schemas/PortDTO'
        areas:
          type: array
          items:
            type: integer
            format: int32
        shipId:
          type: integer
          description: Ship identifier
          format: int32
        localPricing:
          type: string
        marketId:
          type: integer
          format: int32
        nettPrice:
          type: number
        price:
          type: number
        currency:
          type: string
        prices:
          type: array
          items:
            $ref: '#/components/schemas/CabinPriceDTO'
        taxes:
          type: number
      description: Cruise info
    LineDTO:
      type: object
      properties:
        id:
          type: integer
          description: Identififier
          format: int32
        code:
          type: string
          description: Code
        engine:
          type: string
          description: Engine
        logoUrl:
          type: string
          description: Logo url
        name:
          type: string
          description: Name
        niceUrl:
          type: string
          description: Nice image url
      description: Line info
    MetadataDTO:
      type: object
      properties:
        ships:
          type: array
          description: Ships
          items:
            $ref: '#/components/schemas/ShipDTO'
        lines:
          type: array
          description: Lines
          items:
            $ref: '#/components/schemas/LineDTO'
      description: Metadata info
    PortDTO:
      type: object
      properties:
        id:
          type: integer
          description: Port identifier
          format: int32
        name:
          type: string
          description: Port name
      description: Port info
    ResponseDTOSearchAvailabilityResponseDTO:
      type: object
      properties:
        response:
          $ref: '#/components/schemas/SearchAvailabilityResponseDTO'
        elapsedTime:
          type: integer
          format: int64
    SearchAvailabilityResponseDTO:
      type: object
      properties:
        cruises:
          type: array
          description: The cruises info.
          items:
            $ref: '#/components/schemas/CruiseDTO'
        metadata:
          $ref: '#/components/schemas/MetadataDTO'
      description: GET .../availability/search response object.
    ShipDTO:
      type: object
      properties:
        id:
          type: integer
          description: Identififier
          format: int32
        code:
          type: string
          description: Code
        name:
          type: string
          description: Name
        imageCaption:
          type: string
          description: Image caption
        imageUrl:
          type: string
          description: Image Url
        smallImageUrl:
          type: string
          description: Small Image Url
        niceUrl:
          type: string
          description: Nice Image Url
        rating:
          type: integer
          description: Ship rating
          format: int32
      description: Ship info